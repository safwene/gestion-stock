package com.example.demo.dao;

import com.example.demo.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface UserRepository extends MongoRepository <User, String> {
    @Query("{'id':?0}")
    User find_id(String id);

    @Query("{'email':?0 ,'password':?1}")
    User login(String email,String password);
}
