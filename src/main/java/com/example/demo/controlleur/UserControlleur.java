package com.example.demo.controlleur;

import com.example.demo.dao.UserRepository;
import com.example.demo.model.Response;
import com.example.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/")
public class UserControlleur {
    @Autowired
    private UserRepository userRepository;
    @GetMapping("/all")
    public List<User> allUser(){
        return userRepository.findAll();
    }
    @PostMapping("/add")
    public User addUser(@RequestBody User user){
        return userRepository.save(user);
    }
    @PutMapping("/update/{id}")
    public User Upadte(@RequestBody User user,@PathVariable String id ){
        user.setId(id);
        return userRepository.save(user);
    }
@DeleteMapping("/delete/{id}")
    public Response delete(@PathVariable String id) {
    Response rs = new Response();
    try {
      userRepository.deleteById(id);
      rs.setState("ok");
    }
    catch (Exception e){
        System.out.println(e.getMessage());
    }
return rs;
}

@GetMapping("/one/{id}")
public User getone(@PathVariable String id){
        return userRepository.find_id(id);
}
@PostMapping("/login")
    public HashMap<String,User> login(@RequestBody User user){
    System.out.println("email "+user.getEmail());
    System.out.println("pws "+user.getPassword());
        HashMap<String,User> hashMap=new HashMap<>();
        try{
          hashMap.put("data",userRepository.login(user.getEmail(), user.getPassword()));
        }
        catch (Exception e){
            System.out.println("error "+e.getMessage());
            hashMap.put("data",null);
        }
        return hashMap;
    }
}
